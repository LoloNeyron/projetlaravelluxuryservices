<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClientsTable extends Migration {

	public function up()
	{
		Schema::create('clients', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('societyName', 255);
			$table->string('typeSociet', 255);
			$table->string('contactName', 255);
			$table->string('poste', 255);
			$table->string('number', 255);
			$table->string('email', 255);
			$table->text('notes');
		});
	}

	public function down()
	{
		Schema::drop('clients');
	}
}